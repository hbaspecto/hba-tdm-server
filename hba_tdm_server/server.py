#
# hba-tdm-server/hba_tdm_server/server.py ---
#

from __future__ import (
    absolute_import,
    print_function,
)

import datetime
import glob
import logging
import os
import platform
import pprint
import random
import subprocess
import sys
import traceback

import flask

from hba_tdm_util import (
    enable_logging_output,
    md5_of_file,
)

from .util import (
    file_readlines,
    file_write,
    tail_get_lines,
    tail_show,
)

#####

enable_logging_output()

LOGGER = logging.getLogger(__name__)

#####

DEBUG = True

#: One of "Darwin", "Linux", "Windows"
#: On non-Windows, we use testing code.
P_SYSTEM = platform.system()

#####

# TODO: remove after debugging.
if DEBUG:
    LOGGER.info("===== sys.version")
    LOGGER.info(sys.version)
    LOGGER.info("===== sys.path")
    for x in sys.path:
        LOGGER.info("  %s", x)
    LOGGER.info("")

#####

#: Path to the checked out repo.
HBA_TDM_SERVER_DIR = os.environ["HBA_TDM_SERVER_DIR"]


def get_HBA_TDM_SERVER_WORKDIRS():
    """Return HBA_TDM_SERVER_WORKDIRS as a list."""
    tmp = os.environ["HBA_TDM_SERVER_WORKDIRS"]
    # Chop up the path to get a list.
    return tmp.split(os.pathsep)


#: A list of TDM working dirs to search for models.
#: The first dir is where the lockfiles are.
HBA_TDM_SERVER_WORKDIRS = get_HBA_TDM_SERVER_WORKDIRS()

#: Where the emme python is.
HBA_TDM_EMME_PYTHON = os.environ["HBA_TDM_EMME_PYTHON"]

# Abort right away if it isnt found.
if not os.path.exists(HBA_TDM_EMME_PYTHON):
    raise ValueError("HBA_TDM_EMME_PYTHON was not found! (value={!r})".format(
        HBA_TDM_EMME_PYTHON))

#: When this file exists, the run is complete.
#: The TDM program should remove and create this file.
TDM_DONE_PATH = os.environ.get("TDM_DONE_PATH")
if not TDM_DONE_PATH:
    TDM_DONE_PATH = os.path.join(HBA_TDM_SERVER_WORKDIRS[0], "TDM_Done.txt")

#: When this file exists, the run in process.
#: The TDM program should remove and create this file.
TDM_LOCK_PATH = os.environ.get("TDM_LOCK_PATH")
if not TDM_LOCK_PATH:
    TDM_LOCK_PATH = os.path.join(HBA_TDM_SERVER_WORKDIRS[0], "TDM_Lock.txt")

#: Where we write the filenames of the last output files
#: That way we can show the output of the current or last run.
TDM_OUTPUTS_PATH = os.path.join(HBA_TDM_SERVER_WORKDIRS[0], "TDM_Outputs.txt")

#####

# The flask "app" object
app = flask.Flask(__name__)

#####


class ServerError(Exception):
    status_code = 400

    def __init__(self,
                 message,
                 status_code=None,
                 payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(ServerError)
def handle_invalid_usage(error):
    response = flask.jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

#####


def as_byte_str(val):
    """Turn the value into a byte string.
    This is needed to pass env vars to subprocess.
    """
    # Recurse when given a list.
    if isinstance(val, (list,)):
        return [as_byte_str(v) for v in val]

    # Cast to string
    val = "{}".format(val)
    val = val.encode("utf-8")
    return val

#####


def tdm_is_done():
    """Return True if TDM is done."""
    # LOGGER.info("tdm_is_done")
    if os.path.exists(TDM_DONE_PATH):
        # LOGGER.info("tdm_is_done: True")
        return True
    return False


def tdm_is_lock():
    """Return True if TDM is locked."""
    # LOGGER.info("tdm_is_lock")
    if os.path.exists(TDM_LOCK_PATH):
        # LOGGER.info("tdm_is_lock: True")
        return True
    return False


def reap_children_posix():
    """Gather up dead child proceses on posix systems.
    """
    try:
        while True:
            junk = os.waitpid(-1, os.WNOHANG)
            if junk[0] == 0:
                return
    except (OSError,) as e:
        pass


def reap_children_windows():
    """TODO
    """
    pass


def reap_children():
    if P_SYSTEM in ["Darwin", "Linux"]:
        return reap_children_posix()
    if P_SYSTEM in ["Windows"]:
        return reap_children_windows()
    raise ValueError("reap children")

#####


@app.route('/')
def hello():
    """Show a pretty home page for the server.
    Include a bunch of the server state for debgging.
    """
    html = """
<pre>
 _     _                 _      _
| |__ | |__   __ _      | |_ __| |_ __ ___        ___  ___ _ ____   _____ _ __
| '_ \| '_ \ / _` |_____| __/ _` | '_ ` _ \ _____/ __|/ _ \ '__\ \ / / _ \ '__|
| | | | |_) | (_| |_____| || (_| | | | | | |_____\__ \  __/ |   \ V /  __/ |
|_| |_|_.__/ \__,_|      \__\__,_|_| |_| |_|     |___/\___|_|    \_/ \___|_|


HBA_TDM_SERVER_DIR={HBA_TDM_SERVER_DIR}
HBA_TDM_SERVER_WORKDIRS={HBA_TDM_SERVER_WORKDIRS}

TDM_DONE_PATH={TDM_DONE_PATH}
TDM_IS_DONE={TDM_IS_DONE}

TDM_LOCK_PATH={TDM_LOCK_PATH}
TDM_IS_LOCK={TDM_IS_LOCK}

Repo: <a href="https://bitbucket.org/hbaspecto/hba-tdm-server">https://bitbucket.org/hbaspecto/hba-tdm-server</a>

sys.version----------
{version}

sys.path----------
{path}

</pre>

""".format(
        path="\n".join(sys.path),
        version=sys.version,
        HBA_TDM_SERVER_DIR=HBA_TDM_SERVER_DIR,
        HBA_TDM_SERVER_WORKDIRS=HBA_TDM_SERVER_WORKDIRS,
        TDM_DONE_PATH=TDM_DONE_PATH,
        TDM_IS_DONE=tdm_is_done(),
        TDM_LOCK_PATH=TDM_LOCK_PATH,
        TDM_IS_LOCK=tdm_is_lock())
    return html


@app.route("/tdm_status", methods=["GET", "POST"])
def tdm_status():
    # TODO: Commented out while we debug.
    #       Restore when things are working again.
    # reap_children()

    #
    status_paths = file_readlines(TDM_OUTPUTS_PATH)
    stderr_lines = None
    stderr_path = None
    stdout_lines = None
    stdout_path = None
    if 2 <= len(status_paths):
        #
        stderr_path = status_paths[0].strip()
        stderr_lines = tail_get_lines(stderr_path, line_cnt=6)
        #
        stdout_path = status_paths[1].strip()
        stdout_lines = tail_get_lines(stdout_path, line_cnt=6)
        #
        tail_show(stderr_path, lines=stderr_lines)
        tail_show(stdout_path, lines=stdout_lines)

    return flask.jsonify(
        stderr_lines=stderr_lines,
        stderr_path=stderr_path,
        stdout_lines=stdout_lines,
        stdout_path=stdout_path,
        tdm_is_done=tdm_is_done(),
        tdm_is_done_path=TDM_DONE_PATH,
        tdm_is_lock=tdm_is_lock(),
        tdm_is_lock_path=TDM_LOCK_PATH,
        tdm_outputs_path=TDM_OUTPUTS_PATH)


@app.route("/tdm_precheck", methods=["POST"])
def tdm_precheck():

    # peel off the args
    years = flask.request.form.get("years")
    scenario = flask.request.form.get("scenario")

    # use the first dir by default.
    cwd = HBA_TDM_SERVER_WORKDIRS[0]

    precheck_program = [
        HBA_TDM_EMME_PYTHON,
        os.path.join(cwd, "tdm_precheck.py"),
        scenario,
        years,
    ]

    return flask.jsonify(
        #err="Program failed to run.",
        precheck_program=precheck_program)

    output_name = _output_file_name("precheckOutput-{}")

    try:
        p = subprocess.Popen(
            precheck_program,
            cwd=cwd,
            # ensure other FDs are closed.
            close_fds=True,
            # but these ones are open.
            stdin=None,
            stderr=subprocess.PIPE,
            stdout=open(output_name + ".stdout", "w")
        )

        _, stderr = p.communicate()
        error_message = None if p.returncode == 0 else stderr
    except:
        LOGGER.info("tdm_precheck: failed")
        traceback.print_exc()
        #
        return flask.jsonify(
            err="Program failed to run.",
            precheck_program=precheck_program)

    return flask.jsonify(err=error_message)


def _output_file_name(template):
    yyyymmdd = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
    return template.format(yyyymmdd)

#####


def find_model_path(model_name):
    """Search HBA_TDM_SERVER_WORKDIRS for the dir return full path when found."""

    for tmp_path in HBA_TDM_SERVER_WORKDIRS:
        model_path = os.path.join(tmp_path, model_name)
        if os.path.isdir(model_path):
            # We found it!
            return model_path

    # awww...
    return None


def compute_file_name(name, year, scenario):
    """Given the kind, year and scenario, figure out the path
    and filename for it.

    name = The kind of file we are computing.
           (employment, population, taztrips, logfile, skimfile)

    year = 4-digit year as a string.
    scenario = lowercased scenario as paul uses lowercase.
    """

    model_name = "Model-{year} - Production - {scenario}".format(year=year, scenario=scenario)
    LOGGER.debug("model_name=%s", model_name)

    model_path = find_model_path(model_name)
    LOGGER.debug("model_path=%s", model_path)

    if not model_path:
        raise ServerError("Didnt find the model: %s", model_name)

    # these are INPUT files
    if name == "employment":
        # e.g. WORKIR\Model-2016 - Production - T199\Demand\Employment Base 2016.csv
        file_path = os.path.join(
            model_path,
            "Demand",
            "Employment Base {year}.csv".format(year=year))
        LOGGER.info("Upload Employment file is: %s", file_path)
    elif name == "population":
        # e.g. WORKIR\Model-2016 - Production - T199\Demand\SyntheticPopulation2016.csv
        file_path = os.path.join(
            model_path,
            "Demand",
            "SyntheticPopulation{year}.csv".format(year=year))
        LOGGER.info("Upload Population file is: %s", file_path)
    elif name == "taztrips":
        # e.g. WORKIR\Model-2016 - Production - T199\Demand\tazTrips2016.csv
        file_path = os.path.join(
            model_path,
            "Demand",
            "tazTrips{year}.csv".format(year=year))
        LOGGER.info("Upload tazTrips file is: %s", file_path)

    # these are OUTPUT files
    elif name == "logfile":
        # "Model-2016 - Production - i192a/Runlog_16_1.02_i192a_2019-08-01_1533.log"
        # The logfile has a varible name, we need to look for it.
        # where it was created.
        # get all of them.
        glob_pat = os.path.join(
            model_path,
            "Runlog_16_*_{}_*.log".format(scenario.lower()))
        LOGGER.info("model_path=%s", model_path)
        LOGGER.info("listdir:", os.listdir(model_path))
        all_log_files = glob.glob(glob_pat)
        LOGGER.info("glob_pat=%s", glob_pat)
        LOGGER.info("all_log_files=%s", all_log_files)

        # didnt find any!
        if not all_log_files:
            return None
        # pick out the last one by date.
        logfile_path = all_log_files[-1]
        LOGGER.info("Logfile is: %s", logfile_path)
        return logfile_path

    elif name == "skimfile":
        # e.g. WORKIR\Model-2016 - Production - T199\Demand\SDPTM\Outputs\AA logsums.csv"
        file_path = os.path.join(
            model_path,
            "Demand",
            "SDPTM",
            "Outputs",
            # Calculate_AA_logsums.py writes to thei sfile, as specified in settings.py
            "AA logsums_{year}_{scenario}.csv".format(year=year, scenario=scenario),
        )
        LOGGER.info("Skimfile is: %s", file_path)

    # these are NO files
    else:
        raise ValueError("Error: name has invalid value")

    # debugging
    LOGGER.debug(file_path)

    return(file_path)


@app.route(
    "/file_upload",
    methods=['POST'])
def upload_file():
    """Handle an uploaded file."""
    if tdm_is_lock():
        raise ServerError(
            "We think TDM is locked. Check the server!",
            status_code=403)

    if flask.request.method != "POST":
        LOGGER.info("upload_file: ERROR: not a POST.")
        raise ServerError(
            "Not a post!",
            status_code=500)

    #
    scenario = flask.request.args.get("scenario")
    year = flask.request.args.get("year")
    name = flask.request.args.get("name")

    original_file_size = flask.request.headers.get("original-file-size")
    original_file_md5 = flask.request.headers.get("original-file-md5")

    if (name is None) or (year is None) or (scenario is None):
        raise ServerError(
            "Missing required args! scenario={!r} year={!r} name={!r}".format(
                scenario,
                year,
                name),
            status_code=500)

    file_path = compute_file_name(name, year, scenario)

    LOGGER.info("upload_file: path=%s", file_path)

    file_path_tmp = file_path+".tmp"

    BUF_SIZE = (1024*1024)

    with open(file_path_tmp, "wb") as fh:
        while True:
            data = flask.request.stream.read(BUF_SIZE)
            # LOGGER.info("upload_file: data=%s", data)
            # done?  (check len, not b"" or "")
            if len(data) == 0:
                break
            fh.write(data)

    # check our work.
    if original_file_size is not None:
        original_file_size = int(original_file_size)
        file_path_tmp_size = os.path.getsize(file_path_tmp)
        if original_file_size == file_path_tmp_size:
            LOGGER.info("upload_file: SIZE OK: %s==%s",
                        original_file_size,
                        file_path_tmp_size)
        else:
            raise ValueError("File size is bad! orig!=tmp %s!=%s",
                             original_file_size,
                             file_path_tmp_size)

    if original_file_md5 is not None:
        file_path_tmp_md5 = md5_of_file(file_path_tmp)
        if original_file_md5 == file_path_tmp_md5:
            LOGGER.info("upload_file: MD5 OK: %s==%s",
                        original_file_md5,
                        file_path_tmp_md5)
        else:
            raise ValueError("File size is bad! orig!=tmp %s!=%s",
                             original_file_md5,
                             file_path_tmp_md5)

    # put in final place.
    if os.path.exists(file_path):
        os.remove(file_path)
    os.rename(file_path_tmp, file_path)
    file_size = os.path.getsize(file_path)

    return flask.jsonify(
        ok=True,
        file_size=file_size,
        dst_path=file_path)


@app.route("/start_tdm_script", methods=["POST"])
def start_tdm_script():
    """Start the TDM model; dont wait for it to finish.
    The trick is not to use "subprocess.run" as that will wait;
    TDM takes hours to run.
    We want to return right away.

    Only one TDM can be running at a time.

    The tdm_script_name should be something like:

    "_cmd_ASET_TDM_LOOP_v2.0.py"
    "_cmd_ASET_TDM_LOOP_v2.1_COVID.py"

    in which case it is looked for in the "./Scripts" dir.

    Or an absolute path to the script, which is used as is.

    """

    if tdm_is_lock():
        raise ServerError(
            "We think TDM is emlocki-ed. Check the server!",
            403)

    data = dict(flask.request.json)
    year = data["year"]
    scenario = data["scenario"]
    # Called "name" in args, but will be the path.
    tdm_script_path = data["script_name"]

    # "Model-2016 - Production - i192a"
    model_name = "Model-{year} - Production - {scenario}".format(
        year=year,
        scenario=scenario)

    model_path = find_model_path(model_name)

    if not model_path:
        raise ServerError("Didnt find the model: {!r}".format(model_name))

    # dont change if an abspath...
    if not os.path.isabs(tdm_script_path):
        tdm_script_path = os.path.join(
            model_path,
            "Scripts",
            tdm_script_path)

    if not os.path.exists(tdm_script_path):
        raise ServerError("Didnt find the script: {!r}".format(tdm_script_path))

    # output_name is used before the "subprocess cwd", so join it
    output_name = _output_file_name("tdmOutput-{}")

    # We want to be sure emme is running the script,
    # so we pass the script as an arg to emme python.
    # (Dont call the script directly, we dont know which python will run it.)
    TDM_PROGRAM = [
        HBA_TDM_EMME_PYTHON,
        tdm_script_path,
    ]

    stderr_path = os.path.join(model_path, output_name + ".stderr")
    stdout_path = os.path.join(model_path, output_name + ".stdout")

    LOGGER.info("YEAR        = %s", year)
    LOGGER.info("SCENARIO    = %s", scenario)
    LOGGER.info("MODEL_PATH  = %s", model_path)
    LOGGER.info("STDERR_PATH = %s", stderr_path)
    LOGGER.info("STDOUT_PATH = %s", stdout_path)
    LOGGER.info("TDM_PROGRAM = %s", TDM_PROGRAM)

    # Add our env vars, so the subprocess knows the path to the lock files.
    # see "./examples/tdm_script_examples.py"
    tdm_script_env = dict(os.environ)
    tdm_script_env.update({
        "TDM_YEAR": as_byte_str(year),
        "TDM_SCENARIO": as_byte_str(scenario),
        "TDM_DONE_PATH": as_byte_str(TDM_DONE_PATH),
        "TDM_LOCK_PATH": as_byte_str(TDM_LOCK_PATH),
    })

    try:
        # The trick is not to use "subprocess.run" as that will wait;
        # We want to return right away.
        proc = subprocess.Popen(
            as_byte_str(TDM_PROGRAM),
            cwd=as_byte_str(model_path),
            env=tdm_script_env,
            stderr=open(stderr_path, "w"),
            stdin=None,
            stdout=open(stdout_path, "w"))

        # discard the proc
        proc = None

    except (BaseException,) as e:
        LOGGER.info("### start_tdm_script: failed: %s", TDM_PROGRAM)
        traceback.print_exc()
        # failed!
        return flask.jsonify(
            ok=False,
            tdm_program=TDM_PROGRAM)

    # Write the paths, so we can get them back later.
    file_write(TDM_OUTPUTS_PATH, [
        stderr_path,
        stdout_path,
    ])

    return flask.jsonify(ok=True)


@app.route("/download_file", methods=["POST"])
def download_file():
    """Download a file from the server.

    The client asks for the kind(name) and we call
    ``compute_file_name`` to get the path for that kind of
    file and return it.
    """
    # LOGGER.info("download_file")
    # pprint.pLOGGER.info(list(flask.request.form.items()))

    # TODO: compute the name from this.
    name = flask.request.form["name"]
    year = flask.request.form["year"]
    scenario = flask.request.form["scenario"]

    file_path = compute_file_name(name, year, scenario)

    LOGGER.info("Downloading file: %s", file_path)

    if not file_path or not os.path.exists(file_path):
        raise ServerError(
            "ERROR: no path: {!r}".format(file_path),
            404)
    LOGGER.info("file_path is: %s", file_path)

    '''# Make it relative
    if file_path.startswith(HBA_TDM_SERVER_WORKDIR):
        file_path = os.path.join(
            ".",
            file_path[len(HBA_TDM_SERVER_WORKDIR)+1:])
'''
    LOGGER.info("download_file: %s", file_path)

    '''return flask.send_from_directory(
        HBA_TDM_SERVER_WORKDIR,
        file_path,
        mimetype="application/octet-stream")
'''
    return flask.send_file(file_path, mimetype="application/octet-stream")
