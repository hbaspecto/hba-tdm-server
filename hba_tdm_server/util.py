#
# hba-tdm-server/hba_tdm_server/util.py ---
#

"""Utility functions for the server.
"""

from __future__ import (
    absolute_import,
    print_function,
)

import logging

LOGGER = logging.getLogger(__name__)

#####

DEFAULT_TAIL_LINE_CNT = 10

#####


def file_readlines(path):
    """Read all the lines of the file at PATH."""
    try:
        with open(path, "r") as fh:
            return fh.readlines()
    except:
        return []


def file_write(path, data):
    """Write the data or lines to PATH."""
    if isinstance(data, (str,)):
        pass
    else:
        data = "\n".join(data)+"\n"
    #
    with open(path, "w") as fh:
        fh.write(data)
    return True

#####


def tail_get_lines(path, line_cnt=None):
    """Get the last N lines of the file.
    We are working with small files, so we are going to be
    sloppy about this.
    """
    line_cnt = line_cnt or DEFAULT_TAIL_LINE_CNT
    try:
        with open(path, "r") as fh:
            lines = fh.readlines()
        return lines[-line_cnt:]
    except (BaseException,):
        return None


def tail_show(path, line_cnt=None, lines=None):

    if not lines:
        lines = tail_get_lines(path, line_cnt)

    LOGGER.info("========== tail: %s ==========", path)
    if lines:
        for line in lines:
            LOGGER.info(line.rstrip())
    LOGGER.info("==========")
