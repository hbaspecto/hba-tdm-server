#
# hba-tdm-server/setup.py ---
#

from __future__ import (
    absolute_import,
)

import os
from distutils.core import (
    setup,
)

#####

setup(
    name="hba-tdm-server",
    version="0.0.1",
    description="hba-tdm-server and a test client.",
    author="hbaspecto",
    url="https://bitbucket.org/hbaspecto/hba-tdm-server",
    install_requires=[
        "flask",
        "gunicorn",
        "requests",
    ],
    packages=[
        "hba_tdm_client",
        "hba_tdm_server",
        "hba_tdm_util",
    ],
    # where to find the src.
    package_dir={
        'hba_tdm_client': 'hba_tdm_client',
        'hba_tdm_server': 'hba_tdm_server',
        'hba_tdm_util': 'hba_tdm_util',
    },
    entry_points={
        'console_scripts': [
            "hba-tdm-client = hba_tdm_client.hba_tdm_client_main:main_entry",
        ],
    },
)
