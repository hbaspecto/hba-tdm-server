#
# hba-tdm-server/Makefile.inc ---
#

# Usage: include ./Makefile.inc

ifeq (${HBA_TDM_SERVER_DIR},)
  $(error source ./hba-setup.env)
endif

__include_default:
	@echo "Makefile.inc: The first target should be before 'Makefile.inc'."
	exit 1

SHELL:=bash
.SUFFIXES:
