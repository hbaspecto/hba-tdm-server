#
# hba-tdm-server/hba_tdm_util/__init__.py ---
#

from __future__ import (
    absolute_import,
    print_function,
)

import hashlib
import logging
import sys

#####


def sys_flush():
    """Flush stderr and stdout."""
    sys.stdout.flush()
    sys.stderr.flush()

#####


def md5_of_file(path):
    """Compute the MD5 of a file.

    We didnt put into ``util`` as we want a one-file copy-to-update
    to other parts of the code.
    """
    BUF_SIZE = (1024*1024)

    h = hashlib.md5()

    with open(path, "rb") as fh:
        while True:
            data = fh.read(BUF_SIZE)
            if not data:
                break
            h.update(data)

    return h.hexdigest()

#####


def enable_logging_output():
    """Enable debugging output.

    This only works with python3.
    """
    logging.basicConfig(
        format="%(asctime)s | %(message)s",
        datefmt="%Y%m%dT%H%M%S"
    )

    # this is for our logs.
    logging.getLogger().setLevel(logging.DEBUG)

    # fiddle with the logging of "requests"
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    # requests_log.setLevel(logging.INFO)
    requests_log.propagate = True
    # enable the debugging logging.
    import http.client
    http.client.HTTPConnection.debuglevel = 1
