rem
rem  hba-tdm-server/bin/hba_tdm_server.bat ---
rem
rem this batch file creates the appropriate environment
rem and runs the flask server on a windows machine.
rem
rem ***********************************************

rem Where the source is checked out.
set HBA_TDM_SERVER_DIR=C:\PECASASET\hba-tdm-server

rem So python can find it.
set PYTHONPATH=%HBA_TDM_SERVER_DIR%;%PYTHONPATH%

rem where the TDM code is
rem set HBA_TDM_SERVER_WORKDIR=X:\ASET Working\1.0 Model Development\Area 1.2. TDM
set HBA_TDM_SERVER_WORKDIRS=C:\PECASASET\hba-tdm-server

rem where EMME python is
set HBA_TDM_EMME_PYTHON=C:\Program Files\INRO\Emme\Emme 4\Emme-4.4.4.2\Python27\python.exe

rem We are having a problem with some files being truncated.
rem Perhaps this is due to their size? (1.3GiB+)
rem set TMPDIR to this dir, as it ought to have more space than "C:\"

rem set TMPDIR=%HBA_TDM_SERVER_WORKDIR%\tmp
set TMPDIR=C:\PECASASET\hba-tdm-server\tmp
mkdir "%TMPDIR%"

rem ====================
rem
rem Shouldnt need to change the rest.
rem
rem Run the code with a flask dev server.

rem Run with the flask dev server.
rem This gives us better debugging than waitress does.
set FLASK_ENV=development
set FLASK_APP=hba_tdm_server.server:app

"C:\Program Files\INRO\Emme\Emme 4\Emme-4.4.4.2\Python27\python.exe" ^
  -m flask ^
  run ^
  --host 0.0.0.0 ^
  --port 5000
