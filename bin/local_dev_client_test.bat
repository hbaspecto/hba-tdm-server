set PYTHONPATH=X:\ASET Working\1.0 Model Development\hba-tdm-server;%PYTHONPATH%

rem python -m "X:\ASET Working\1.0 Model Development\hba-tdm-server\hba_tdm_client" ^
python -m "hba_tdm_client" ^
  --year 2030 ^
  --scenario X999 ^
  --upload-employment "./README.rst" ^
  --upload-population "./Makefile" ^
  --upload-taztrips   "./Makefile.inc" ^
  --start-tdm ^
  --wait-for-tdm ^
  --download-logfile  "junk-file.log" ^
  --download-skimfile "junk-skims.csv" 
