rem
rem .\bin\windy_dev_client_test.bat
rem
rem Run "windy_dev_server.bat" in one window, then this test command
rem in another CMD window to test it.
rem

.\ve-win\Scripts\python.exe -m "hba_tdm_client" ^
  --year 2030 ^
  --scenario X999 ^
  --upload-employment "./README.rst" ^
  --upload-population "./Makefile" ^
  --upload-taztrips   "./Makefile.inc" ^
  --start-tdm ./test/bin/hba-tdm-fake-tdm-run.py ^
  --wait-for-tdm ^
  --download-logfile  "junk-file.log" ^
  --download-skimfile "junk-skims.csv"
  
