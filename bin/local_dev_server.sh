#!/bin/bash
#
# hba-tdm-server/bin/hba_dev_server.sh ---
#

export SCRIPT_DIR=$(builtin cd $(dirname ${BASH_SOURCE[0]}) ; builtin pwd)

#####

#
export FLASK_ENV=development
export FLASK_APP=hba_tdm_server.server:app

if [[ "${HBA_TDM_FLASK_PORT}" = "" ]]
then
  echo "### ${0}: HBA_TDM_FLASK_PORT is unset!"
  exit 1
fi

# work in the work dir...
cd "${HBA_TDM_SERVER_DIR}"

# With "0.0.0.0", the server will listen from connections from everywhere.

# --no-debugger
# --without-threads

exec ./ve/bin/python -m flask \
  run \
  --eager-loading \
  --host 0.0.0.0 \
  --port ${HBA_TDM_FLASK_PORT}
