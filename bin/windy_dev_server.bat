rem
rem hba-tdm-server/bin/windy_tdm_server_flask.bat ---
rem
rem this batch file creates the appropriate environment
rem and runs the flask server on a windows machine.
rem
rem D:\ASET\TDM\hba-tdm-server\bin\goa_test_server.bat
rem
rem ***********************************************

rem change to the dir above this file.
cd %~dp0/..

rem Where the source is checked out.
set HBA_TDM_SERVER_DIR=C:\HBA\hba-tdm-server

rem The top of the TDM working directories
rem the subdirs will be named like:
rem   "Model-2016 - Production - i192a"
rem Can be a list of dirs, will check in order
rem AAA;BBB;CCC
set HBA_TDM_SERVER_WORKDIRS=C:\HBA\hba-tdm-server/test/fake-server-workdir

rem The emme TDM will run.
rem set HBA_TDM_EMME_PYTHON=C:\Program Files\INRO\Emme\Emme 4\Emme-4.3.7\Python27\python.exe
rem
rem not installed on windy, fake it with the normal python
set HBA_TDM_EMME_PYTHON=C:\Python39\python.exe

rem ====================
rem Shouldnt ened to change below here.
rem 

rem In directory (aka the 'package') "hba_tdm_server", 
rem runs "server.py" with the "app" variable
rem set FLASK_ENV=development
set FLASK_APP=hba_tdm_server.server:app

rem --no-debugger

".\ve-win\Scripts\python.exe" ^
  -m flask ^
  run ^
  --eager-loading ^
  --host 0.0.0.0 ^
  --port 5000
