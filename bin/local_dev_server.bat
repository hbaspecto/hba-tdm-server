rem ***********************************************
rem this batch file creates the appropriate environment
rem and runs the flask server
rem ***********************************************

set FLASK_APP=server.py 
set FLASK_ENV=development
python -m flask run