rem 
rem  hba-tdm-server/bin/coe_prd_client_test.bat ---
rem
rem We want to upload a file, to see that it works on the goa machine.
rem

rem change to the dir above this file.
cd %~dp0/..

set HBA_TDM_SERVER_DIR=D:\ASET\TDM\hba-tdm-server

rem do the upload with a big file.
".\ve-win\Scripts\python.exe" -m hba_tdm_client ^
  --year 2016 ^
  --scenario I282 ^
  --upload-population ^
  "README.rst"
