#!/bin/bash
#
# hba-tdm-server/bin/hba_tdm_client_test.sh ---
#
# Replicates "hba_tdm_client_test.bat"
# But on linux, so we can test there.
#
# The server must be already running.

export SCRIPT_DIR=$(builtin cd $(dirname ${BASH_SOURCE[0]}) ; builtin pwd)
export REPO_DIR=$(dirname ${SCRIPT_DIR})

#####

set PYTHONPATH="${REPO_DIR}:${PYTHONPATH}"

# YEAR and SCENARIO must match in hba-tdm-fake-tdm-run.py
export HBA_TDM_FAKE_YEAR="2030"
export HBA_TDM_FAKE_SCENARIO="T999"

# Only sleep 30 seconds.
export HBA_TDM_FAKE_SLEEP=30

# ensure working dirs exist!
mkdir -p "${HBA_TDM_FAKE_WORKDIR}/Model-${HBA_TDM_FAKE_YEAR} - Production - ${HBA_TDM_FAKE_SCENARIO}/Demand/"

# clean up status files.
#rm -f ./fake-tdm-workdir/TDM_*.txt
# and logfiles
#rm -rf ./fake-tdm-workdir/tdmOutput-*

#####

# run it!
./ve/bin/python -m "hba_tdm_client" \
  --year ${HBA_TDM_FAKE_YEAR} \
  --scenario ${HBA_TDM_FAKE_SCENARIO} \
  --upload-employment "./README.rst" \
  --upload-population "./Makefile" \
  --upload-taztrips   "./Makefile.inc" \
  --start-tdm "${HBA_TDM_FAKE_SCRIPT}" \
  --wait-for-tdm \
  --download-logfile  "junk-file.log" \
  --download-skimfile "junk-skims.csv"

# Local Variables:
# mode: ksh
# End:
