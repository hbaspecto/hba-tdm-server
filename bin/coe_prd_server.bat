rem
rem  hba-tdm-server/bin/hba_tdm_server.bat ---
rem
rem this batch file creates the appropriate environment
rem and runs the flask server on a windows machine.
rem
rem D:\ASET\TDM\hba-tdm-server\bin\goa_test_server.bat
rem
rem ***********************************************

rem change to the dir above this file.
cd %~dp0/..

rem Where the source is checked out.
set HBA_TDM_SERVER_DIR=C:\PECASASET\hba-tdm-server

rem The top of the TDM working directory.
rem the subdirs will be name like:
rem   "Model-2016 - Production - i192a"
set HBA_TDM_SERVER_WORKDIR=C:\PECASASET

rem The emme TDM will run.
set HBA_TDM_EMME_PYTHON=C:\Program Files\INRO\Emme\Emme 4\Emme-4.4.4.2\Python27\python.exe

rem ====================
rem Shouldnt ened to change below here.
rem 

rem In directory (aka the 'package') "hba_tdm_server", 
rem runs "server.py" with the "app" variable
set FLASK_ENV=development
set FLASK_APP=hba_tdm_server.server:app

set PYTHONUNBUFFERED=1

rem removed: --eager-loading
rem Full path to the script.
".\ve-win\Scripts\python.exe" ^
  -m flask ^
  run ^
  --without-threads ^
  --no-debugger ^
  --no-reload ^
  --host 0.0.0.0 ^
  --port 5000
  
