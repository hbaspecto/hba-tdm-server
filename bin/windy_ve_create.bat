rem 
rem hba-tdm-server/bin/windy_ve_create.bat ---
rem
rem builds the VE with Python-39.
rem
rem Copy this to "XXX_ve_create.bat" for your site,
rem and change it to suit.

rem change to the dir above this file.
cd %~dp0/..

rem build a VE in this dir.
"C:\Python39\python.exe" -m venv ^
  ve-win

rem load the CMD env up. (use 'call' instead of 'source' like bash.)
call .\ve-win\Scripts\activate.bat

rem now install stuff into ve-win.
".\ve-win\Scripts\python.exe" -m pip ^
   install ^
  "flask" "paste" "requests"

rem now install hba-tdm-server
".\ve-win\Scripts\python.exe" -m pip ^
   install ^
  --editable "."
