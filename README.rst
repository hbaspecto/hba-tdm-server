hba-tdm-server/README
==================================================

This is the TDM server which runs on a Windows host which
has EMME installed.

The server runs on Pauls Windows machine.  The client runs
on the Linux run host where ``run_pecas.py`` is run.

At the CoE, they do much the same, but with the ``coe_*`` scripts.


Links:

- Repo:         https://bitbucket.org/hbaspecto/hba-tdm-server
- Docs:         http://docs.office.hbaspecto.com/hba-tdm-server
- Jenkins:      http://jenkins-2.office.hbaspecto.com/job/hba-tdm-server
- TDM scripts:  https://bitbucket.org/hbaspecto/aset_tdm

- COE: (city-of-edmonton)
  - PRD: http://WIN_IP:5000/



Quickstart / Linux
--------------------------------------------------

The linux server can only be used for testing.
``EMME`` isnt available on linux, but we can run scripts
which fake it.

::

    git clone git@bitbucket.org:hbaspecto/hba-tdm-server.git
    cd hba-tdm-server
    source ./hba-setup.env
    make _test


Quickstart / Windows @ HBA
--------------------------------------------------

The HBA PRD server is Pauls desktop, ``longview``.  Before
running a TDM model, check that the server is running.

- PRD: http://longview.office.hbaspecto.com:5000


Installation steps:

- Start with Win-10 and Emme installed.

- Install Git-Bash.
  https://gitforwindows.org/

- Install notepad++
  https://notepad-plus-plus.org/downloads/

- Install Python-3.
  https://www.python.org/downloads/release

  When installing, install it to "C:\Python39"
  And do NOT update the system path.
  Leave the system env vars for EMME to manage.

- Add all the other HBA scripts.
  Like: https://bitbucket.org/hbaspecto/aset_tdm

- Check this repo out with SourceTree or git bash.
  Recommended location is:
  "C:\HBA\hba-tdm-server"

- Adjust the paths in the ``hba_tdm_server.bat`` file.

- Run ``hba_tdm_server.bat`` from the ``bin`` directory

- HBA_TDM_EMME_PYTHON=C:\Program Files\INRO\Emme\Emme 4\Emme-4.3.5\Python27\python.exe


Quickstart / Windows @ City-of-Edmonton
--------------------------------------------------

At HBA, we do this:

- check out the repo and make a zip file of the
  ``hba-tdm-server`` and Pauls scripts.

- Put the zip files at ``whitemud:/zfs/www/files-hbaspecto-com/edmonton``.

- Better yet, ``git commit`` so we can ``git pull`` on the client computer.


Then, on the CoE windows machine:

- Install the same SW we have at HBA.

- Edit the system env to have the path to the Emme ``pip``.

- Close and open a new ``cmd`` to get the updated ``Path``.

- Download the zips and unpack them.

- ``cd`` to the install directory.

- Copy ``windy_ve_create.bat`` to ``coe_ve_create.bat``.
  Edit it to have the correct paths and run it to create the VE.

- Copy ``windy_tdm_server_flask.bat`` to ``coe_tdm_server.bat``
  and change its config to have the correct paths.
  Something like this:

::

    set HBA_TDM_SERVER_WORKDIR=D:\ASET\TDM\Sep 2019 Version
    set HBA_TDM_EMME_PYTHON=C:\Program Files\INRO\Emme\Emme 4\Emme-4.3.7\Python27\python.exe

- Change the other scripts to have the correct emme
  version. (``4.3.7``) and the correct scenario
  numbers. (???)

- Run ``.\bin\coe_tdm_server.bat`` and minimize the window.

- Change the pecas run config to have the Ip address of the
  EMME computer.

- Extra: change the ".py" assoc to use the correct Emme python


Updating the code @ CoE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- open a CMD window

::

    # use the "d:" drive
    D:
    # go to the source dir
    cd \ASET\TDM\hba-tdm-server

    # update the source
    git pull

    # rebuild the VE
    .\bin\coe_ve_create.bat

    # Now run the server
    .\bin\coe_tdm_server.bat



Locking
--------------------------------------------------

We would like to have the TDM computer to be shared between
runs.

- the standard integrated run (Q241)

- the alternative integrated run (P243)

When one run makes a request, it will wait for the other to
finish.

The pecas runhost will wait until the server says it is done.

The change will be to have the runhost wait, not error out.





hba-tdm-client
--------------------------------------------------

``hba-tdm-client`` is a CLI used to test the server with.
What it does should be incoprated into ``run_pecas.py``.

The options in the example are the order they will be run
in.  (If you change the order of the options, they still run
in the order of this example.)

::

    hba-tdm-client \
      --year 2016 \
      --scenario i192a \
      --upload-employment "./test-data/Employment Base 2016.csv" \
      --upload-population "./test-data/Outputsamples2016.csv" \
      --upload-taztrips   "./test-data/tazTrips2016.csv" \
      --run-tdm \
      --wait-for-tdm \
      --download-logfile  "junk-file.log" \
      --download-skimfile "junk-skims.csv"


NOTES
--------------------------------------------------

- Emme uses Python 2.7.14

- There can only be ONE instance of the TDM program running.

- The ``TDM`` program needs to communicate its state via making files.

  - ``TDM_Done.txt`` = created at the end of a TDM run.

  - ``TDM_Lock.txt`` = created at the start of a TDM run and removed at the end.

  If the TDM program crashes, then these files will need to be removed by a human.

- Sometimes hosts dont have their DNS setup correctly.  Thus
  if you use a hostname, it cant resolve the name and you
  get a connection error.


Interface with TDM scripts
--------------------------------------------------

These vars are set in the env for the scripts to use.

- ``HBA_TDM_SERVER_DIR`` = Where the server us running.

- ``HBA_TDM_SERVER_WORKDIRS`` = list of dirs to search for projects.

- ``TDM_DONE_PATH`` = Path to tdm done file.

- ``TDM_LOCK_PATH`` = Path to tdm lock file.



Running I192ii
--------------------------------------------------

Procedure for running on ``I192ii`` for modeling at AT.

- The client and server run different python versions.

  - client runs

Start the server if not already running.
Leave it running in its own command window.

::

    D:
    cd D:\ASET\TDM\hba-tdm-server
    .\bin\goa-tdm-server.bat



Now start a run.
In a different window:

::

    D:
    cd D:\ASET\SEM\I192ii
    "C:\Program files\Python37\python.exe" .\runpecas.py

This should check the server is running, when it prints these lines:

::

    root  : INFO  {'tdm_is_done': False, ...

And run for some time.
