#
# hba-tdm-server/Makefile ---
#

_default: _test

include ./Makefile.inc

#####

# for OSX systems, with "brew" installed.
_brew_install:
	brew install python

#####

sys_python_exe:=$(shell PATH=/usr/local/bin:/usr/bin:${PATH} type -p python3)
pip_cmd:=${HBA_TDM_SERVER_VE_DIR}/bin/python -m pip

_ve_build:
	mkdir -p "$(dir ${HBA_TDM_SERVER_VE_DIR})"
#
	${sys_python_exe} -m venv "${HBA_TDM_SERVER_VE_DIR}"
# only need these for DEV work.
ifeq (${HBA_TDM_DEPLOY},dev)
	${pip_cmd} install --upgrade pip
	${pip_cmd} install --upgrade autopep8 isort pylint
endif
#
	${pip_cmd} install -e .

_ve_rm:
	-rm -rf "${HBA_TDM_SERVER_VE_DIR}"

_ve_rebuild: _ve_rm _ve_build

${HBA_TDM_SERVER_VE_DIR}:
	make _ve_rebuild

_ve_exists: ${HBA_TDM_SERVER_VE_DIR}

#####

_clean:
	-rm -f junk-*.{csv,log,txt}
	-rm -f tdmOutput-*.std{err,out}
	-rm -f upload-{employment,population,taztrips}-*-*

_clean_pyc:
	find . -name \*.pyc -print -delete

_clean_fake_tdm_workdir:
	find ./fake-tdm-workdir -name \*.std\* -print -delete

_dos2unix:
	dos2unix ./bin/*.bat ./*/*.py

autopep8_files+=$(wildcard bin/*.py)
autopep8_files+=$(wildcard hba_tdm_client/*.py)
autopep8_files+=$(wildcard hba_tdm_server/*.py)
autopep8_files+=$(wildcard hba_tdm_util/*.py)
autopep8_files+=$(wildcard setup.py)
autopep8_files+=$(wildcard test/*.py)

_isort:
	isort \
	  --multi-line 3 \
	  --trailing-comma \
	  --line-width 1 \
	  ${autopep8_files}

# "--aggressive" messes up the server banner page.
_autopep8:
	autopep8 \
	  --in-place \
	  --max-line-length 120 \
	  ${autopep8_files}

_pylint:
	-pylint --py3k ${autopep8_files}

# Dont rebuild the ve, removing python kills the test server.
#_precommit+=_ve_rebuild
#_precommit+=_clean
#_precommit+=_dos2unix
_precommit+=_isort
_precommit+=_autopep8
_precommit+=_pylint
#_precommit+=_test

_precommit: ${_precommit}

#####

_docs_all _docs_rsync_to_swdocs:
	cd sphinx && make ${@}

#####

opt_debug:=
#opt_debug+= --debug

# You can use other test data - this just happens to be mine.
_get_test_data:
	rsync -Pa \
	  hail:/Users/harley/repos/hba/hba-tdm-server/test-data/ \
	  ./test-data/

#####

SCENARIO_DIR:=${HBA_TDM_FAKE_WORKDIR}/Model-${HBA_TDM_FAKE_YEAR} - Production - ${HBA_TDM_FAKE_SCENARIO}

# Run this before the tests.
_run_server: _fake_workdir_clean
#
	mkdir -p "${SCENARIO_DIR}"
#
	./bin/${HBA_TDM_CLIENT}_${HBA_TDM_DEPLOY}_server.sh

#
_fake_workdir_clean:
	find "${HBA_TDM_FAKE_WORKDIR}" -name "*.stderr"  -print -delete
	find "${HBA_TDM_FAKE_WORKDIR}" -name "TDM_*.txt" -print -delete

_fake_workdir_show_output:
	find "${HBA_TDM_FAKE_WORKDIR}" -type f | sort
#
	find "${HBA_TDM_FAKE_WORKDIR}" \
	\( -name "*.stderr" -o -name "*.stdout" -o -name "TDM_*.txt" \) \
	-exec head -n 100 \{\} \;

#####

unittest_args+=--failfast
unittest_args+=-v

_test_unittest:
	cd ./test && \
	python -m unittest \
	   ${unittest_args} \
	   test_*.py

#####

_test_help:
	hba-tdm-client --help

_test_run_get_tdm_status:
	hba-tdm-client --get-tdm-status

_test_run_wait_for_tdm:
	hba-tdm-client --wait-for-tdm

#####

# 1 GiB
# large_file_size:=1073741824
# 2 GiB
large_file_size:=2147483648

large_file_name=./test-large-file-${large_file_size}.data

${large_file_name}:
	head -c ${large_file_size} /dev/zero > ${@}.tmp
	mv ${@}.tmp ${@}
#
	ls -lh ${@}

_test_upload_largefile: | ${large_file_name}
# clean out all the CSVs
	-rm "${SCENARIO_DIR}"/Demand/*.csv
# upload a really big file as a test.
	hba-tdm-client \
	  --year              "${HBA_TDM_FAKE_YEAR}" \
	  --scenario          "${HBA_TDM_FAKE_SCENARIO}" \
	  --upload-employment "${large_file_name}"
#
	ls -lh "${SCENARIO_DIR}"/Demand/*.csv

#####

_test_err_503:
	hba-tdm-client \
	  --server-url "http://httpbin.org/status/503" \
	  --get-tdm-status

#####

# A quick test with small files.
_test_run_quick:
#
	-rm -f ${HBA_TDM_FAKE_WORKDIR}/TDM_{Done,Running}.txt
	-rm -rf "${SCENARIO_DIR}"
#
	mkdir -p "${SCENARIO_DIR}/Demand"
#
	hba-tdm-client ${opt_debug} \
	  --year              "${HBA_TDM_FAKE_YEAR}" \
	  --scenario          "${HBA_TDM_FAKE_SCENARIO}" \
	  --upload-employment "./README.rst" \
	  --upload-population "./Makefile" \
	  --upload-taztrips   "./Makefile.inc" \
	  --start-tdm         "${HBA_TDM_FAKE_SCRIPT_PY}" \
	  --wait-for-tdm \
	  --download-logfile  "junk-file.log" \
	  --download-skimfile "junk-skims.csv"

_test_run_download_files:
	hba-tdm-client \
	  --year ${HBA_TDM_FAKE_YEAR} \
	  --scenario ${HBA_TDM_FAKE_SCENARIO} \
	  --download-logfile  "junk-logfile" \
	  --download-skimfile "junk-skimfile"

# Real file sizes.
# Dont run this unless Paul is ready to test.
_test_run_longview:
#
	rm -f ./fake-tdm-run/TDM_{Done,Running}.txt
#
	hba-tdm-client \
	  --year ${HBA_TDM_FAKE_YEAR} \
	  --scenario ${HBA_TDM_FAKE_SCENARIO} \
	  --upload-employment "./test/fixtures/Employment Base 2016.csv" \
	  --upload-population "./test/fixtures/Outputsamples2016.csv" \
	  --upload-taztrips   "./test/fixtures/tazTrips2016.csv" \
	  --start-tdm         "./Scripts/_cmd_ASET_TDM_LOOP_v2.0.py" \
	  --wait-for-tdm \
	  --download-logfile  "junk-file.log" \
	  --download-skimfile "junk-skims.csv"

_run_hba_tdm_fake_script.py:
# cleanup
	rm -f ${TDM_DONE_PATH} ${TDM_LOCK_PATH}
# These are the vars which the server exports.
	TDM_SCENARIO=${HBA_TDM_FAKE_SCENARIO} \
	TDM_YEAR=${HBA_TDM_FAKE_YEAR} \
	./ve/bin/python \
	  ${HBA_TDM_FAKE_SCRIPT_PY}

#####

# test with curl so we can have a 2nd method to test the server.
_test_curl_tdm_status_get:
	curl -v \
	  "${HBA_TDM_SERVER_URL}/tdm_status"

_test_curl_tdm_status_post:
	curl -v \
	--data-urlencode @/dev/null \
	"${HBA_TDM_SERVER_URL}/tdm_status"

#####

_test+=_ve_exists
_test+=_fake_workdir_clean
_test+=_test_unittest
_test+=_test_help
_test+=_test_run_quick
_test+=_test_upload_largefile

_test: ${_test}

#####

# We are using a copy of tdmclient in "I192ii".
# This target updates that copy, then the remote users
# Can pull it for testing.
_I192ii_update_tdmclient:
#
	(cd ../Scenarios/I192ii && svn up)
#
	cp ./hba_tdm_client/tdmclient.py ../Scenarios/I192ii
#
	(cd ../Scenarios/I192ii && svn commit -m "Update tdmclient." ./tdmclient.py)
