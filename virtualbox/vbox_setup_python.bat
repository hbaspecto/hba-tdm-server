rem 
rem  hba-tdm-server/virtualbox/vbox_setup_python.bat ---
rem 
rem When running this, be sure to click "add to path" in the installer.
rem

cd %~dp0

mkdir installers

powershell -Command "[Net.ServicePointManager]::SecurityProtocol = 'tls12, tls11'; (New-Object Net.WebClient).DownloadFile('https://www.python.org/ftp/python/3.7.5/python-3.7.5-amd64.exe', '.\installers\python-3.7.5-amd64.exe')"

.\installers\python-3.7.5-amd64.exe

rem Now exit this cmd window and make a new cmd windows
rem so you have the new PATH with python
rem
rem Then run "vbox_setup_pip"
