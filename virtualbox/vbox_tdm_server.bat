rem
rem  hba-tdm-server/bin/vbox_tdm_server.bat ---
rem

rem CD to the dir above this one.
cd %~dp0/..

set HBA_TDM_SERVER_DIR=%cd%

rem ==========

set FLASK_APP=hba_tdm_server.server:app
set FLASK_ENV=development

rem So python can find it.
set PYTHONPATH=%HBA_TDM_SERVER_DIR%;%PYTHONPATH%

rem where the TDM code is
set HBA_TDM_SERVER_WORKDIR=%HBA_TDM_SERVER_DIR%\fake-tdm-workdir

rem The dev server.
rem
rem python -m flask ^
rem   run ^
rem   --eager-loading ^
rem   --host 127.0.0.1 ^
rem   --port 5000

rem The waitress server.
rem
rem waitress-serve --listen 0.0.0.0:5000 "hba_tdm_server.server:app"
waitress-serve ^
  --listen 0.0.0.0:5000 ^
  --threads 10 ^
  --call "hba_tdm_server.server:waitress_server"

