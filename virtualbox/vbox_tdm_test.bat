rem
rem  hba-tdm-server/virtualbox/vbox_tdm_test.bat ---
rem

rem CD to the dir above this one.
cd %~dp0/..

set HBA_TDM_SERVER_DIR=%cd%

rem ==========

del fake-tdm-workdir\TDM_Done.txt
del fake-tdm-workdir\TDM_Lock.txt

rem python -m hba_tdm_client --help

rem python -m hba_tdm_client --get-tdm-status

python -m hba_tdm_client --start-tdm --wait-for-tdm

