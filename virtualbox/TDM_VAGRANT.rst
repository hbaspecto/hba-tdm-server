TDM_VAGRANT
==================================================

When debugging the install on windows, use this VM.



Setup / Install
--------------------------------------------------

- https://www.virtualbox.org/wiki/Downloads
  You might need 6.0.14, as vagrant doesnt like ``6.1.0``.
  https://download.virtualbox.org/virtualbox/6.0.14/

- https://www.vagrantup.com/downloads.html

::

    make _vagrant_up

Once the host has been started:

- Windows updates

- Open Virtualbox UI

- Install Python 37

At this point, try and snapshot the image with VirtualBox so
you dont have to do the installs and Windows Service pack
updates again.

Running the server
--------------------------------------------------

NOTE: When rebooting the windows box, the drives might not
be mounted.  Remount them by "Mapping a network drive" from
the file explorer.  Mount "\\VBOXSVR\hba_tdm_server" as
"D:".

- Start a "CMD" window

- Change dirs to the ``hba_tdm_server`` dir

::

    D:
    cd /

- Install the packages from pip

::

    .\virtualbox\vbox_setup_pip.bat

- Start the server

::

    .\virtualbox\vbox_tdm_server.bat

- In another CMD window:

::

    .\virtualbox\vbox_tdm_test.bat
