rem 
rem  hba-tdm-server/virtualbox/vbox_setup_pip.bat ---
rem 
rem run after "vbox_setup_python"

cd %~dp0/..

pip install "flask" "requests" "waitress"
