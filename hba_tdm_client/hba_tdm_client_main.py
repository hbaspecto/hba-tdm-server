#
# hba-tdm-server/hba_tdm_client/hba_tdm_client_main.py ---
#

from __future__ import (
    absolute_import,
    print_function,
)

import argparse
import logging
import pdb
import pprint
import sys

from hba_tdm_util import (
    enable_logging_output,
)

from .tdmclient import (
    TdmClient,
)

#####

enable_logging_output()

LOGGER = logging.getLogger(__name__)

#####


def main(raw_args):
    """
    The main for ``tdm-server``.

    Handle the args and use :py:class:`hba_tdm_client.tdmclient.TdmClient`
    to make the requests.
    """

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.description = """
DESCRIPTION:

FIXME

"""
    parser.epilog = """
EXAMPLES:

See: http://docs.office.hbaspecto.com/hba-tdm-server

"""

    g = parser.add_argument_group('GENERAL')

    g.add_argument("--debug", "-d",
                   action="store_true",
                   help=argparse.SUPPRESS)
    g.add_argument("--pdb",
                   action="store_true",
                   help=argparse.SUPPRESS)
    g.add_argument("--verbose", "-v",
                   action="store_true",
                   help="Be more verbose.")

    g = parser.add_argument_group(
        "TDM_SERVER",
        description="FIXME")

    g.add_argument("--server-url",
                   help="Url to the server.")

    g.add_argument("--year", "-y",
                   help="Year of scenario. YEAR")
    g.add_argument("--scenario", "-s",
                   help="Scenario name. X999")

    g.add_argument("--upload-employment",
                   help="Employment file to upload. (input)")
    g.add_argument("--upload-population",
                   help="Population file to upload. (input)")
    g.add_argument("--upload-taztrips",
                   help="Taz Trips to upload. (input)")

    g.add_argument("--start-tdm",
                   help="Start the TDM run with this SCRIPT.")

    g.add_argument("--get-tdm-status",
                   action="store_true",
                   help="Get the current run status.")

    g.add_argument("--precheck",
                   action="store_true",
                   help="Do TDM prechecks.")

    g.add_argument("--wait-for-tdm",
                   action="store_true",
                   help="Wait for the TDM run to finish.")

    g.add_argument("--download-logfile",
                   help="Fetch the logfile to LOGFILE. (output)")

    g.add_argument("--download-skimfile",
                   help="fetch the skims to skimfile. (output)")

    args = parser.parse_args(raw_args)

    if args.pdb:
        pdb.set_trace()

    if args.debug:
        import hba_tdm_client.tdmclient
        hba_tdm_client.tdmclient.enable_debugging_output()

    tdmclient = TdmClient(
        server_url=args.server_url,
        year=args.year,
        scenario=args.scenario)

    status = tdmclient.tdm_status()
    if status["tdm_is_lock"]:
        LOGGER.info("TDM is running.")
        LOGGER.info("Lockfile is %s", status.get("tdm_is_lock_path"))
        return False

    if args.get_tdm_status:
        pprint.pprint(tdmclient.tdm_status())
    if args.precheck:
        pprint.pprint(tdmclient.tdm_precheck(args.scenario, args.year))

    if args.upload_employment:
        tdmclient.upload_employment(args.upload_employment)
    if args.upload_population:
        tdmclient.upload_population(args.upload_population)
    if args.upload_taztrips:
        tdmclient.upload_taztrips(args.upload_taztrips)

    if args.start_tdm:
        tdmclient.start_tdm_script(args.start_tdm)

    if args.wait_for_tdm:
        tdmclient.wait_for_tdm()

    if args.download_logfile:
        tdmclient.download_logfile(args.download_logfile)
    if args.download_skimfile:
        tdmclient.download_skimfile(args.download_skimfile)

    return True


def cast_rv(rv):
    """
    Cast a python return value to a unix (single byte) return value.

    :param rv: int
    """
    if sys.stdout:
        sys.stdout.flush()
    if sys.stderr:
        sys.stderr.flush()
    if rv is True:
        return 0
    if rv is None or rv is False:
        return -1
    return rv


def main_entry():
    """
    The entry point for main.
    Used to cast the return value to a numeric value.
    """
    sys.exit(cast_rv(main(sys.argv[1:])))


if __name__ == "__main__":
    main_entry()
