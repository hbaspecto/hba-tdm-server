#
# hba-tdm-server/hba_tdm_client/tdmclient.py ---
#
# Note: This is a copy of "tdmclient.py"
#       If you make changed here, please merge them into
#       the original at:
# https://bitbucket.org/hbaspecto/hba-tdm-server/src/master/hba_tdm_client/tdmclient.py
#
# - The client runs with the python from PECAS. (python-3.xx)
# - The server with its own ./ve-win/Python-3.9
# - The EMME program runs with its bundled python-2.7
#
# Keep this library as something which can be copied/installed into PECAS.
#

from __future__ import (
    absolute_import,
    print_function,
)

import datetime
import hashlib
import logging
import os
import pprint
import socket
import sys
import time
import traceback

import requests
import requests.exceptions
import urllib3

from hba_tdm_util import (
    md5_of_file,
)

#####

LOGGER = logging.getLogger(__name__)

#####

#: URL to the server we will talk to.
HBA_TDM_SERVER_URL = os.environ.get(
    "HBA_TDM_SERVER_URL",
    "http://127.0.0.1:5000")

#####


class TdmClient(object):
    """A client for the TDM server.
    Use TdmClient to drive the TDM server.
    """

    def __init__(self,
                 year=None,
                 scenario=None,
                 server_url=None):
        self._year = year
        self._scenario = scenario
        self._server_url = server_url or HBA_TDM_SERVER_URL
        self._wait_for_tdm_delay = 60
        self._wait_for_tdm_retries = 3
        #
        self._server_url = self._server_url.rstrip("/")

        # Create our own http session...
        self._session = requests.Session()
        # Add some headers.
        # self._session.headers.update({
        #    'Connection': 'close',
        # })
        # Set up our own adapter, which has pooling turned off.
        custom_adapter = requests.adapters.HTTPAdapter(
            # ask urllib3 to retry
            # (this is in addition to our own retries.)
            # max_retries=1,
            # pool_connections=1,
            # pool_maxsize=1)
        )
        self._session.mount("http://", custom_adapter)

        # check we have the correct adapters settings.
        # pprint.pprint(self._session.adapters["http://"].__dict__)

        # ...and turn off keep_alive.
        # This only works with newer versions of request, which we dont have.
        # self._session.config['keep_alive'] = False

        #: Time to sleep between consectuive errors.
        self._err_sleep_time = 10
        #: Number of consectuive errors to retry.
        self._err_retry_max = 5

    def __repr__(self):
        return "#<{} {:#x}>".format(
            self.__class__.__name__,
            id(self))

    ###

    def post(self, *args, **kwargs):
        """A wrapper for requests.post, where we retry a couple of times.

        If we get some errors, retry them a couple of times before throwing an error.

        We only use the connection once (Connection: close)
        We want to add "headers = {"Connection": "close"}" to kwargs.
        Because windows OS will close connections on us, resulting in an error.
        (Requests thinks the socket is open, but it was closed behind it back.)
        So we use the connection once, and explicitly close it.
        """

        rv = None
        err_cnt = 0

        # ... add "Connection: close" if it isnt already set.
        kwargs = dict(kwargs)
        if "headers" not in kwargs:
            kwargs["headers"] = {}
        kw_headers = kwargs["headers"]
        # if "Connection" not in kw_headers:
        #    kw_headers["Connection"] = "close"
        reply = self._session.post(
            *args,
            **kwargs)

        LOGGER.info("tdm_post: reply.status_code=%s",
                    reply.status_code)
        return reply

#        while True:
#            # ensure our prior connections are closed.
#            self._session.close()
#
#            try:
#                # for debugging, do this
#                # raise requests.exceptions.ConnectionError()
#
#                # use our session, which has keep_alive disabled.
#                reply = self._session.post(
#                    *args,
#                    **kwargs)
#
#                if reply.status_code != 200:
#                    LOGGER.error("tdm_post: reply.status_code=%s",
#                                 reply.status_code)
#                return reply
#
#            except (
#                    # We are listing a bunch of the superclasses here, as somehow
#                    #   requests.exceptions.ConnectionError
#                    # isnt being caught.
#                    BaseException,
#                    IOError,
#                    requests.exceptions.RequestException,
#                    requests.exceptions.ConnectionError,
#                    socket.error,
#            ) as e:
#                # skip
#                if isinstance(e, (KeyboardInterrupt,)):
#                    LOGGER.error("tdm_post: %s", e)
#                    raise
#                # a note for the user and debugging.
#                if isinstance(e, (requests.exceptions.ConnectionError,)):
#                    LOGGER.error("tdm_post: ConnectionError: Is the remote tdm-server running?")
#                elif isinstance(e, (requests.exceptions.RequestException,)):
#                    LOGGER.error("tdm_post: RequestException: A generic server error.")
#                elif isinstance(e, (BaseException,)):
#                    LOGGER.error("tdm_post: BaseException: this shouldnt really happen. %s", e)
#                elif isinstance(e, (socket.error,)):
#                    LOGGER.error("tdm_post: socket.error: %s", e)
#                else:
#                    LOGGER.error("tdm_post: Unhandled error: %s", e)
#
#                # Retry, or reraise?
#                err_cnt += 1
#                if self._err_retry_max < err_cnt:
#                    #
#                    print("TdmClient: server_url={!r} ==========".format(self._server_url))
#                    # reraise the error, as we didnt get it to work.
#                    traceback.print_exc()
#                    raise
#                # sleep a bit and retry.
#                time.sleep(self._err_sleep_time)

    ###

    def tdm_status(self):
        """Get the status of the TDM run on the server.
        There can only be one TDM program running at at time.
        """

        LOGGER.info("tdm_status")

        url = self._server_url+"/"+"tdm_status"

        #
        rv = self.post(
            url,
            data={
                "year": self._year,
                "scenario": self._scenario,
            })

        if not rv:
            raise ValueError("tdm_status: Server didnt return a valid response.")

        data = rv.json()
        LOGGER.info(pprint.pformat(data))
        #
        if rv:
            rv.close()
        return data

    def tdm_precheck(self, scenario, years):
        """
        Do pre-run checks on the travel model setup
        """

        LOGGER.info("tdm_precheck")

        url = self._server_url + "/" + "tdm_precheck"

        rv = self.post(
            url,
            data={
                "scenario": scenario,
                "years": years
            }
        )
        data = rv.json().get("err")
        if rv:
            rv.close()
        return data

    def upload_file(self,
                    src_path,
                    name=None,
                    dst_path=None):
        """Upload a file to the server.
        """
        LOGGER.info("upload_file: %s %s", name, src_path)

        url = self._server_url+"/"+"file_upload"

        # uncompressed file size.
        original_file_size = os.path.getsize(src_path)
        # MD5 of the uncompressed file.
        original_file_md5 = md5_of_file(src_path)

        if True:
            LOGGER.info("upload_file: src_path=%s size=%s md5=%s",
                        src_path,
                        original_file_size,
                        original_file_md5)

        # The old way was to POST as form, with the file attached.
        # The new was is to send the args as url params,
        # and the data as the body of the POST.
        with open(src_path, "rb") as payload:
            rv = self.post(
                url,
                data=payload,
                headers={
                    # "application/gzip" in the future.
                    'content-type': 'application/octet-stream',
                    'original-file-size': '{}'.format(original_file_size),
                    'original-file-md5': original_file_md5,
                },
                params={
                    "year": self._year,
                    "scenario": self._scenario,
                    "dst_path": dst_path,
                    "src_path": src_path,
                    "name": name,
                })

        LOGGER.info("rv=%s", rv)

        # might be misformatted.
        try:
            pprint.pprint(rv.json())
        except:
            pass

        if rv:
            rv.close()
        return True

    def upload_employment(self, src_path):
        """Upload the employment file.
        """
        LOGGER.info("upload_employment")
        self.upload_file(
            name="employment",
            src_path=src_path)

    def upload_population(self, src_path):
        LOGGER.info("upload_population")
        self.upload_file(
            name="population",
            src_path=src_path)

    def upload_taztrips(self, src_path):
        LOGGER.info("upload_taztrips")
        self.upload_file(
            name="taztrips",
            src_path=src_path)

    def start_tdm_script(self, script_name):
        """Ask the server to start the TDM program on the server.
        Abort if TDM is running already.

        The script_name is looked for in "./Scripts" in the model.
        """

        url = self._server_url+"/start_tdm_script"

        rv = self.post(
            url,
            json={
                "script_name": script_name,
                "scenario": self._scenario,
                "year": self._year,
            })
        LOGGER.info("rv=%s", rv)

        if rv:
            rv.close()
        return True

    def wait_for_tdm(self):
        """Poll the server for TDM to complete.
        Ask for the status until we find TDM_DONE.
        """
        failures = 0

        while True:
            LOGGER.info("wait_for_tdm")
            time.sleep(self._wait_for_tdm_delay)

            status = self.tdm_status()

            if status["tdm_is_done"]:
                break
            if status["tdm_is_lock"]:
                continue

            failures += 1
            if failures >= self._wait_for_tdm_retries:
                raise TravelModelFailure(
                    "The DONE and LOCK files are both missing; check the status of the travel model"
                )

    def download_file(self, name, dst_path):
        """Download a file from the server.
        Stream it to the destination file, as it will be big.
        """
        LOGGER.info("download_file: %s %s", name, dst_path)
        url = self._server_url+"/"+"download_file"
        chunk_size = 1024 * 1024
        with self.post(
                url,
                data={
                    "year": self._year,
                    "scenario": self._scenario,
                    "name": name,
                },
                stream=True) as req:

            # When a 404, print a better message:
            if req.status_code == 404:
                LOGGER.fatal(
                    "download_file: error=404; find the file for: year=%s scenario=%s name=%s",
                    self._year,
                    self._scenario,
                    name)
            # abort on any error.
            req.raise_for_status()

            with open(dst_path+".tmp", 'wb') as fh:
                for chunk in req.iter_content(chunk_size=chunk_size):
                    if chunk:
                        fh.write(chunk)
            os.rename(dst_path+".tmp", dst_path)
        #
        LOGGER.info("download_file: OK")

    def download_logfile(self, dst_path):
        LOGGER.info("download_logfile: %s", dst_path)
        return self.download_file(
            "logfile",
            dst_path)

    def download_skimfile(self, dst_path):
        LOGGER.info("download_skimfile: %s", dst_path)
        return self.download_file(
            "skimfile",
            dst_path)


class TravelModelFailure(Exception):
    """An exception used when the travel model has failed."""
    pass
