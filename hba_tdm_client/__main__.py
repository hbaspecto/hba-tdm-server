#
# hba-tdm-server/hba_tdm_client/__main__.py ---
#
# So we can do "python -m hba_tdm_client ..."

from __future__ import (
    absolute_import,
    print_function,
)

from .hba_tdm_client_main import (
    main_entry,
)

main_entry()
