#
# hba-tdm-server/hba_tdm_client/__init__.py ---
#

"""

hba_tdm_client
----------------------------------------

- :py:mod:`hba_tdm_client.tdmclient` 

"""

from __future__ import (
    absolute_import,
    print_function,
)

from .tdmclient import (
    TdmClient,
)
