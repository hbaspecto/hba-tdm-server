#
# hba-tdm-server/test/test_fixtures.py ---
#

from __future__ import (
    absolute_import,
)

import os

#####

HBA_TDM_SERVER_DIR = os.environ.get("HBA_TDM_SERVER_DIR")

FIXTURES_DIR = os.path.join(
    HBA_TDM_SERVER_DIR,
    "test",
    "fixtures")

HBA_TDM_FAKE_SCRIPT_PY = os.environ.get("HBA_TDM_FAKE_SCRIPT_PY")

LOCAL_DEV_SERVER_SH = os.path.join(HBA_TDM_SERVER_DIR, "bin", "local_dev_server.sh")
