#
# hba-tdm-server/test/test_big_file_upload.py ---
#
#

from __future__ import (
    absolute_import,
)

import unittest

from hba_tdm_client import (
    hba_tdm_client_main,
)

#####


class TestBigFileUpload(unittest.TestCase):

    @unittest.skip("add the big files to test.")
    def test_big_file_1(self):
        for i in range(20):
            args = [
                "-y", str(4200 + i),
                "-s", "I225i",
                "--upload-employment", "Employment.csv",
                "--upload-population", "Outputsamples_income.csv",
                "--upload-taztrips", "tazTrips.csv"
            ]
            hba_tdm_client_main.main(args)

#####


if __name__ == '__main__':
    unittest.main()
