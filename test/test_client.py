#
# hba-tdm-server/test/test_client.py ---
#

from __future__ import (
    absolute_import,
    print_function,
)

import os.path
import pdb
import pprint
import subprocess
import unittest

from test_fixtures import *

from hba_tdm_client.tdmclient import (
    TdmClient,
)

#####


def with_running_server(func):
    def with_running_server_wrapped(*args, **kwargs):
        print("with_running_server: start")
        proc = subprocess.Popen([LOCAL_DEV_SERVER_SH])
        try:
            return func(*args, **kwargs)
        finally:
            print("with_running_server: kill")
            proc.kill()
    return with_running_server_wrapped

#####


class TestTdmClient(unittest.TestCase):

    @with_running_server
    def test_main_page(self):
        #
        tdmclient = TdmClient()
        rv = tdmclient.tdm_status()
        print("rv={!r}".format(rv))

#####


if __name__ == '__main__':
    unittest.main()
