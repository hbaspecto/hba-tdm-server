#
# hba-tdm-server/test/test_server.py ---
#

from __future__ import (
    absolute_import,
    print_function,
)

import os
import pdb
import pprint
import unittest

from test_fixtures import *

import hba_tdm_server.server
from hba_tdm_server.server import (
    as_byte_str,
)

#####


class TestTdmServer(unittest.TestCase):

    # Note: the client here is not the "hba_tdm_client"; it is a http client.
    def test_1(self):
        client = hba_tdm_server.server.app.test_client()

    def test_main_page(self):
        client = hba_tdm_server.server.app.test_client()

        rv = client.get("/")
        print(rv)
        self.assertEquals(rv.status_code, 200)

    def test_as_byte_str(self):

        self.assertEquals(
            as_byte_str("abc"),
            b"abc")

        self.assertEquals(
            as_byte_str(["abc"]),
            [b"abc"])

        self.assertEquals(
            as_byte_str([1, 2, 3]),
            [b"1", b"2", b"3"])

#####


if __name__ == '__main__':
    unittest.main()
