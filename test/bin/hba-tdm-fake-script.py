#!/usr/bin/python
#
# hba-tdm-server/test/bin/hba-tdm-fake-script.py ---
#

"""
Fakes the run of the TDM program on a linux machine.
Written in python so it can be used as a test script on windows and linux.

It has example code to put into your TDM scripts.

"""

from __future__ import (
    absolute_import,
    division,
    print_function,
)

import datetime
import os
import shutil
import sys
import time

#####

def get_HBA_TDM_SERVER_WORKDIRS():
    """Get HBA_TDM_SERVER_WORKDIRS as a list."""
    tmp = os.environ.get("HBA_TDM_SERVER_WORKDIRS")
    if tmp:
        tmp = tmp.split(os.pathsep)
    return tmp

#####


def get_TDM_DONE_PATH():
    """Gets the path of TDM_DONE_PATH.
    This is set by the server.
    When unset, it isnt being used.
    """
    tmp = os.environ.get("TDM_DONE_PATH")
    return tmp


def rm_TDM_DONE_PATH():
    """Removes TDM_DONE, if it exists."""
    path = get_TDM_DONE_PATH()
    if path:
        if os.path.exists(path):
            os.remove(path)
    return True


def write_TDM_DONE_PATH(data):
    path = get_TDM_DONE_PATH()
    if path:
        with open(path, "w") as fh:
            fh.write(data)
    return True

#####


def get_TDM_LOCK_PATH():
    """Like TDM_DONE_PATH."""
    tmp = os.environ.get("TDM_LOCK_PATH")
    return tmp


def is_locked_TDM_LOCK_PATH():
    """Return True if the TDM is locked."""
    lock_path = get_TDM_LOCK_PATH()
    # Paul - if not running under the server, (by hand)
    # should the TDM be considered unlocked?
    if lock_path is None:
        return False
    #
    if os.path.exists(lock_path):
        return True
    #
    return False


def lock_TDM_LOCK_PATH():
    lock_path = get_TDM_LOCK_PATH()
    if lock_path:
        with open(lock_path, "w") as fh:
            fh.write("tdm is locked.")
    return True


def unlock_TDM_LOCK_PATH():
    """Lock the TDM."""
    lock_path = get_TDM_LOCK_PATH()
    if lock_path:
        if os.path.exists(lock_path):
            os.remove(lock_path)
    return True


#####

HBA_TDM_SERVER_DIR = os.environ["HBA_TDM_SERVER_DIR"]

# for quicker testing...
# export HBA_TDM_FAKE_SLEEP=10
HBA_TDM_FAKE_SLEEP = int(os.environ.get("HBA_TDM_FAKE_SLEEP", 120))

TDM_INPUT_FILES = [
]

#
TDM_SCENARIO = os.environ.get("TDM_SCENARIO")
TDM_YEAR = os.environ.get("TDM_YEAR")

#####


def printenv():
    for (key, val) in sorted(os.environ.items()):
        print("{}={!r}".format(
            key, val))


def gen_run_name(scenario, year):
    name = "Model-{} - Production - {}".format(
        year,
        scenario)

    return name


def gen_logfile_name(scenario, now=None):
    now = now or datetime.datetime.now()
    return now.strftime("./Runlog_16_1.02_{}_%Y-%m-%d_%H%M.log".format(
        scenario.lower()))


def file_write(path, data):
    """Write DATA to PATH.
    A handy function for faking output.
    """
    #
    path_dir = os.path.dirname(path)
    # 2.7 doesnt have "exist_ok=True"
    try:
        os.makedirs(path_dir)
    except:
        pass
    #
    print("### echo {!r} > {!r}".format(data, path))
    with open(path, "w") as fh:
        fh.write(data)


def file_rm(path):
    try:
        print("### rm {!r}".format(path))
        os.remove(path)
    except (OSError,) as e:
        pass


def main_entry():
    """This fakes the run of a TDM script.
    """

    printenv()

    if is_locked_TDM_LOCK_PATH():
        print("### Already running.")
        sys.exit(1)
    # start

    lock_TDM_LOCK_PATH()

    work_dir = get_HBA_TDM_SERVER_WORKDIRS()[0]
    run_name = gen_run_name(TDM_SCENARIO, TDM_YEAR)
    run_path = os.path.join(work_dir, run_name)

    # Where the log will be.
    logfile_name = gen_logfile_name(TDM_SCENARIO)
    # dump in current directory.
    logfile_path = os.path.join(run_path, logfile_name)

    # We are starting, so not done.
    rm_TDM_DONE_PATH()
    file_rm(logfile_path)

    # test for inputs.
    ok = True
    for input_file in TDM_INPUT_FILES:
        if not os.path.exists(input_file):
            print("### missing input file: {}".format(input_file))
            ok = False

    if not ok:
        sys.exit(1)

    # Wait a bit to simulate the awesome speed of EMME.
    print("### Running EMME for {} sec".format(HBA_TDM_FAKE_SLEEP))

    # produce some output so we can see it in the logs.
    idle_steps = 10
    idle_sleep = HBA_TDM_FAKE_SLEEP/idle_steps
    for idx in range(idle_steps):
        print("### {}/{}: sleeping for {}".format(
            idx, idle_steps, idle_sleep))
        sys.stdout.flush()
        time.sleep(idle_sleep)

    # put our faked outputs there.
    print("### Starting copies.")

    #
    file_write(logfile_path, "some fake log data")

    #
    aa_logsums_path = os.path.join(
        run_path,
        "./Demand/SDPTM/Outputs/AA logsums_{}_{}.csv".format(
            TDM_YEAR, TDM_SCENARIO))
    file_write(aa_logsums_path, "fake logsum data")

    #
    taztrips_name = "tazTrips{}.csv".format(TDM_YEAR)
    taztrips_path = os.path.join(
        run_path,
        taztrips_name)
    file_write(taztrips_path,
               "some fake taz data")

    # say we are done before removing the lock file.
    write_TDM_DONE_PATH("done")
    unlock_TDM_LOCK_PATH()


if __name__ == "__main__":
    main_entry()
