"""
This program tests the upload of tazTrips.csv, which often fails if the file is over 1 GB.
The example files are meant to be kept in test/taztrips, but they're ignored in Git because
of their size. Instead, copy them in from the ProjectWork drive, at
Edmonton RTM 2015/Integration testing/A263 upload test files. Copy all the folders in that
directory into test/taztrips.
To run this, the TDM server must be running on Longview, and there must be a scenario
(which can be empty) called T01 in one of the usual TDM locations.
"""

from __future__ import (
    absolute_import,
    print_function,
)

import os

from hba_tdm_client import (
    tdmclient,
)


def main():
    test_dir = join("test", "taztrips")
    for year in [2016, 2020, 2026, 2032, 2039, 2049]:
        client = tdmclient.TdmClient(year, 'T01', "http://longview.office.hbaspecto.com:5000/")
        client.upload_taztrips(os.path.join(test_dir, str(year), "tazTrips.csv"))


if __name__ == "__main__":
    main()
